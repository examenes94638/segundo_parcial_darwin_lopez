from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.shortcuts import render

# Create your views here.
class HomeTemplateView(LoginRequiredMixin, TemplateView):
 template_name = 'home.html'