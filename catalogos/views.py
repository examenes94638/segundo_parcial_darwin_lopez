from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.core.paginator import Paginator
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from .models import Cliente, Asunto, Procurador
from .forms import ClienteForm, AsuntoForm, ProcuradorForm

# Create your views here.
class ClienteListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Cliente
    template_name = 'cliente/cliente_list.html'
    context_object_name = 'clientes'
    paginate_by = 3
    login_url = reverse_lazy('login')
    permission_required = 'catalogos.view_cliente'
   


class ClienteCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'cliente/cliente_form.html'
    success_url = reverse_lazy('clientes_list')
    login_url = reverse_lazy('login')
    permission_required = 'catalogos.add_cliente'


class ClienteUpdateView(LoginRequiredMixin, UpdateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'cliente/cliente_form.html'
    success_url = reverse_lazy('clientes_list')
    login_url = reverse_lazy('login')


class ClienteDeleteView(LoginRequiredMixin, DeleteView):
    model = Cliente
    template_name = 'cliente/cliente_confirm_delete.html'
    success_url = reverse_lazy('clientes_list')
    login_url = reverse_lazy('login')

#---------- procuradors ----------

class ProcuradorListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Procurador
    template_name = 'procurador/procurador_list.html'
    context_object_name = 'procuradores'
    login_url = reverse_lazy('login')
    permission_required = 'catalogos.view_procurador'


class ProcuradorCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Procurador
    form_class = ProcuradorForm
    template_name = 'procurador/procurador_form.html'
    success_url = reverse_lazy('procuradores_list')
    login_url = reverse_lazy('login')
    permission_required = 'catalogos.add_procurador'


class ProcuradorUpdateView(LoginRequiredMixin, UpdateView):
    model = Procurador
    form_class = ProcuradorForm
    template_name = 'procurador/procurador_form.html'
    success_url = reverse_lazy('procuradores_list')
    login_url = reverse_lazy('login')


class ProcuradorDeleteView(LoginRequiredMixin, DeleteView):
    model = Procurador
    template_name = 'procurador/procurador_confirm_delete.html'
    success_url = reverse_lazy('procuradores_list')
    login_url = reverse_lazy('login')

#---------- asuntos ----------

class AsuntoListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Asunto
    template_name = 'asunto/asunto_list.html'
    context_object_name = 'asuntos'
    login_url = reverse_lazy('login')
    permission_required = 'catalogos.view_asunto'


class AsuntoCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Asunto
    form_class = AsuntoForm
    template_name = 'asunto/asunto_form.html'
    success_url = reverse_lazy('asuntos_list')
    login_url = reverse_lazy('login')
    permission_required = 'catalogos.add_asunto'


class AsuntoUpdateView(LoginRequiredMixin, UpdateView):
    model = Asunto
    form_class = AsuntoForm
    template_name = 'asunto/asunto_form.html'
    success_url = reverse_lazy('asuntos_list')
    login_url = reverse_lazy('login')


class AsuntoDeleteView(LoginRequiredMixin, DeleteView):
    model = Asunto
    template_name = 'asunto/asunto_confirm_delete.html'
    success_url = reverse_lazy('asuntos_list')
    login_url = reverse_lazy('login')