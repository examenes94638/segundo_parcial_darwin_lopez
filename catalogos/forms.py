from django import forms
from bootstrap_datepicker_plus.widgets import DatePickerInput
from .models import Cliente, Procurador, Asunto


class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ['dni','nombre','direccion','fecha_nacimiento']
        widgets = {
            'fecha_nacimiento': DatePickerInput()
        }

class ProcuradorForm(forms.ModelForm):
    class Meta:
        model = Procurador
        fields = ['dni', 'casos_ganados','nombre','apellidos','num_colegiado']

class AsuntoForm(forms.ModelForm):
    class Meta:
        model = Asunto
        fields = ['num_expediente', 'fecha_inicio','fecha_fin', 'estado']