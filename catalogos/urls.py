from django.urls import path
from . import views

urlpatterns = [
    path('clientes/', views.ClienteListView.as_view(), name='clientes_list'),
    path('clientes/nuevo/', views.ClienteCreateView.as_view(), name='clientes_create'),
    path('clientes/<int:pk>/', views.ClienteUpdateView.as_view(), name='clientes_update'),
    path('clientes/<int:pk>/eliminar/', views.ClienteDeleteView.as_view(), name='clientes_delete'),
    path('procuradores/', views.ProcuradorListView.as_view(), name='procuradores_list'),
    path('procuradores/nuevo/', views.ProcuradorCreateView.as_view(), name='procuradores_create'),
    path('procuradores/<int:pk>/', views.ProcuradorUpdateView.as_view(), name='procuradores_update'),
    path('procuradores/<int:pk>/eliminar/', views.ProcuradorDeleteView.as_view(), name='procuradores_delete'),
    path('asuntos/', views.AsuntoListView.as_view(), name='asuntos_list'),
    path('asuntos/nuevo/', views.AsuntoCreateView.as_view(), name='asuntos_create'),
    path('asuntos/<int:pk>/', views.AsuntoUpdateView.as_view(), name='asuntos_update'),
    path('asuntos/<int:pk>/eliminar/', views.AsuntoDeleteView.as_view(), name='asuntos_delete'),
]