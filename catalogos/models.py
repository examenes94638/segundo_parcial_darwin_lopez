from django.db import models

# Create your models here.
class Cliente(models.Model):
    dni = models.CharField(max_length=9)
    nombre = models.CharField(max_length=20)
    direccion = models.CharField(max_length=30)
    fecha_nacimiento = models.DateField()

    def __str__(self):
       return self.nombre


class Procurador(models.Model):
    dni = models.CharField(max_length=9)  # Cambiado a una cadena de 9 caracteres para el DNI
    casos_ganados = models.CharField(max_length=5) # Cambiado a un número de 5 dígitos para los casos ganados
    nombre = models.CharField(max_length=20)
    apellidos = models.CharField(max_length=20)  # Cambiado a una cadena de 20 caracteres para los apellidos
    num_colegiado = models.CharField(max_length=5)  # Cambiado a un número de 5 dígitos para el número de colegiado

    def __str__(self):
        return f'{self.nombre} {self.apellidos}'

class Asunto(models.Model):
    num_expediente = models.CharField(max_length=4) 
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    estado = models.CharField(max_length=10)
    def __str__(self):
        return f'Expediente {self.num_expediente} - Estado: {self.estado}'
